## default

#r <- new("ruleSet")
#r2 <- setDefaultLists(r)
#r3 <- readLists(r2)
#r4 <- setDefaultParams(r3)
#r4@polarity='negative'
#r5 <- generateRules(r4)



## Now to make our own


r <- new("ruleSet")
r2 <- setDefaultLists(r) 


basedir='D:/LIFE/git/xcms_general/'
r2@ionlistfile = paste(basedir,'rules_ions.csv',sep='')
r2@neutrallossfile=paste(basedir,'rules_neutralloss.csv',sep='')
r2@neutraladditionfile =paste(basedir,'rules_neutraladdition.csv',sep='')


r3 <- readLists(r2) 
r4 <- setDefaultParams(r3) 

#r4@polarity='negative'
r4@maxcharge=1
r4@mol=5
r4@nnloss=5
r4@nnadd=4 # cannot see it does something
r4@nh=4 # cannot see it does something
r5 <- generateRules(r4)

dim(r5@rules)

View(r5@rules)